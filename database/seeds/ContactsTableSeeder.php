<?php

use Illuminate\Database\Seeder;
use App\Contact;
use Carbon\Carbon;


class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
    	Contact::truncate();
        $faker = \Faker\Factory::create();

    	$contact = new Contact;
        $contact->from = $faker->firstName ." " .$faker->lastName;
        $contact->subject = $faker->sentence;
        $contact->phone = $faker->phoneNumber;
        $contact->email = $faker->email;
        $contact->content = $faker->paragraph;
        $contact->created_at = Carbon::now()->subDays(4);
        $contact->save();

            	$contact = new Contact;
        $contact->from = $faker->firstName ." " .$faker->lastName;
        $contact->subject = $faker->sentence;
        $contact->phone = $faker->phoneNumber;
        $contact->email = $faker->email;
        $contact->content = $faker->paragraph;
        $contact->created_at = Carbon::now()->subDays(4);
        $contact->save();

    	$contact = new Contact;
        $contact->from = $faker->firstName ." " .$faker->lastName;
        $contact->subject = $faker->sentence;
        $contact->phone = $faker->phoneNumber;
        $contact->email = $faker->email;
        $contact->content = $faker->paragraph;
        $contact->created_at = Carbon::now()->subDays(4);
        $contact->save();

    	$contact = new Contact;
        $contact->from = $faker->firstName ." " .$faker->lastName;
        $contact->subject = $faker->sentence;
        $contact->phone = $faker->phoneNumber;
        $contact->email = $faker->email;
        $contact->content = $faker->paragraph;
        $contact->created_at = Carbon::now()->subDays(4);
        $contact->save();



    }
}
