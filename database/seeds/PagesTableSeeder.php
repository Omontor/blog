<?php

use Illuminate\Database\Seeder;
use App\Page;
use Carbon\Carbon;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Page::truncate();

        $page = new Page;
        $page->name = "About";
        $page->url = str_slug($page->name);
        $page->title = "Acerca de";
        $page->content = "<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt odio vitae pretium consectetur. Morbi eleifend lectus eu nunc scelerisque, ac tempor ipsum cursus. Nullam vitae nulla sodales, laoreet eros sed, mollis mauris. Phasellus vel urna tortor. Etiam vitae quam ultrices, tempus tortor non, venenatis nulla. Fusce vehicula ac purus ut fermentum. Nulla condimentum sem quis pretium consectetur. Integer sollicitudin arcu in metus molestie, at laoreet mauris laoreet. Curabitur eu mollis dolor. Sed commodo ut mi vitae gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Mauris elementum convallis nibh, ac placerat nibh dictum eget. Phasellus iaculis interdum neque, quis volutpat tortor finibus sed.
</p>
<p>
Nam et elementum felis, ac efficitur lacus. Integer tristique diam ante. Proin bibendum vitae lorem a aliquet. Sed ut nisl ut elit viverra imperdiet. In elementum neque quis urna hendrerit maximus. Suspendisse tellus erat, euismod non aliquam eget, eleifend eget diam. Donec imperdiet orci non elit viverra, ac posuere lacus varius. Etiam nulla orci, rutrum vitae pulvinar at, pharetra at est. Pellentesque ipsum quam, rhoncus quis imperdiet id, lacinia nec libero. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum elit augue, pulvinar nec pharetra eu, pulvinar sit amet sem. Aliquam scelerisque, arcu eget sollicitudin volutpat, arcu neque dictum elit, vel dictum urna justo sed mauris. Maecenas vehicula vitae purus in iaculis. Vivamus id dictum magna, eu malesuada odio. Duis accumsan nibh at orci condimentum varius.
</p>
<p>
Etiam tempor egestas ligula, viverra convallis magna viverra a. Ut sed lacus id dui aliquam interdum. Integer ullamcorper luctus quam, nec mollis felis ornare sit amet. Duis tristique vel tellus eget fringilla. Fusce mattis velit in elit lobortis, vitae placerat sem tempus. Sed eu magna condimentum eros viverra efficitur eget malesuada orci. Nam in elementum nisl, ac tincidunt erat. Sed elementum eleifend fermentum. Praesent erat odio, molestie et tristique nec, vestibulum facilisis nisl. Vestibulum sit amet dictum tortor. Vivamus vel gravida odio, vitae egestas massa. Phasellus pretium, dolor vitae semper feugiat, dolor mi dignissim enim, maximus efficitur diam diam eget dolor. Nullam a lacus eu est tincidunt pellentesque quis a nibh. Praesent nec sollicitudin tortor. Aenean vulputate sagittis accumsan. Proin sit amet elit sed purus placerat gravida quis sed ligula.
</p>
<p>
Morbi gravida nunc sit amet erat tempor molestie. Curabitur ultricies neque id metus fermentum semper. Donec rhoncus maximus vestibulum. Donec felis erat, tempor quis lectus ut, dictum euismod purus. Pellentesque finibus velit non vehicula blandit. Praesent tellus odio, dictum ut iaculis eget, condimentum sed turpis. Maecenas faucibus purus a nibh feugiat consequat. Vestibulum pulvinar diam in ipsum tristique, sed fringilla est lobortis. Cras dictum sit amet urna eu efficitur. Mauris lacinia tincidunt viverra. Mauris et accumsan urna. Sed ultricies malesuada ipsum sed fermentum. Aenean non ligula vitae felis viverra volutpat.
</p>
<p>
Morbi cursus ex mattis aliquet posuere. Integer quis tempor quam. In eu leo iaculis, hendrerit lorem id, dignissim lectus. Duis augue ipsum, dictum a lacus quis, bibendum ullamcorper velit. Etiam accumsan, diam id efficitur tincidunt, enim turpis rutrum ligula, non mattis ex purus sed magna. Fusce quis magna ac sem tristique vehicula. Maecenas vehicula eget turpis id tincidunt. Maecenas sollicitudin volutpat volutpat. Ut ut nisl vel lacus tempor ornare eu a diam. Nullam semper lacus in dui vulputate convallis. Aenean dignissim tortor tortor, nec rutrum velit ullamcorper nec. Mauris id lectus condimentum, ultricies odio eu, condimentum elit. Ut ut maximus quam, non commodo mi. Cras non tristique enim. Proin bibendum non libero quis viverra.
</p>";
        $page->created_at = Carbon::now()->subDays(4);
        $page->save();




        $page = new Page;
        $page->name = "Privacy Policy";
        $page->url = str_slug($page->name);
        $page->title = "Política de privacidad";
        $page->content = "<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt odio vitae pretium consectetur. Morbi eleifend lectus eu nunc scelerisque, ac tempor ipsum cursus. Nullam vitae nulla sodales, laoreet eros sed, mollis mauris. Phasellus vel urna tortor. Etiam vitae quam ultrices, tempus tortor non, venenatis nulla. Fusce vehicula ac purus ut fermentum. Nulla condimentum sem quis pretium consectetur. Integer sollicitudin arcu in metus molestie, at laoreet mauris laoreet. Curabitur eu mollis dolor. Sed commodo ut mi vitae gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Mauris elementum convallis nibh, ac placerat nibh dictum eget. Phasellus iaculis interdum neque, quis volutpat tortor finibus sed.
</p>
<p>
Nam et elementum felis, ac efficitur lacus. Integer tristique diam ante. Proin bibendum vitae lorem a aliquet. Sed ut nisl ut elit viverra imperdiet. In elementum neque quis urna hendrerit maximus. Suspendisse tellus erat, euismod non aliquam eget, eleifend eget diam. Donec imperdiet orci non elit viverra, ac posuere lacus varius. Etiam nulla orci, rutrum vitae pulvinar at, pharetra at est. Pellentesque ipsum quam, rhoncus quis imperdiet id, lacinia nec libero. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum elit augue, pulvinar nec pharetra eu, pulvinar sit amet sem. Aliquam scelerisque, arcu eget sollicitudin volutpat, arcu neque dictum elit, vel dictum urna justo sed mauris. Maecenas vehicula vitae purus in iaculis. Vivamus id dictum magna, eu malesuada odio. Duis accumsan nibh at orci condimentum varius.
</p>
<p>
Etiam tempor egestas ligula, viverra convallis magna viverra a. Ut sed lacus id dui aliquam interdum. Integer ullamcorper luctus quam, nec mollis felis ornare sit amet. Duis tristique vel tellus eget fringilla. Fusce mattis velit in elit lobortis, vitae placerat sem tempus. Sed eu magna condimentum eros viverra efficitur eget malesuada orci. Nam in elementum nisl, ac tincidunt erat. Sed elementum eleifend fermentum. Praesent erat odio, molestie et tristique nec, vestibulum facilisis nisl. Vestibulum sit amet dictum tortor. Vivamus vel gravida odio, vitae egestas massa. Phasellus pretium, dolor vitae semper feugiat, dolor mi dignissim enim, maximus efficitur diam diam eget dolor. Nullam a lacus eu est tincidunt pellentesque quis a nibh. Praesent nec sollicitudin tortor. Aenean vulputate sagittis accumsan. Proin sit amet elit sed purus placerat gravida quis sed ligula.
</p>
<p>
Morbi gravida nunc sit amet erat tempor molestie. Curabitur ultricies neque id metus fermentum semper. Donec rhoncus maximus vestibulum. Donec felis erat, tempor quis lectus ut, dictum euismod purus. Pellentesque finibus velit non vehicula blandit. Praesent tellus odio, dictum ut iaculis eget, condimentum sed turpis. Maecenas faucibus purus a nibh feugiat consequat. Vestibulum pulvinar diam in ipsum tristique, sed fringilla est lobortis. Cras dictum sit amet urna eu efficitur. Mauris lacinia tincidunt viverra. Mauris et accumsan urna. Sed ultricies malesuada ipsum sed fermentum. Aenean non ligula vitae felis viverra volutpat.
</p>
<p>
Morbi cursus ex mattis aliquet posuere. Integer quis tempor quam. In eu leo iaculis, hendrerit lorem id, dignissim lectus. Duis augue ipsum, dictum a lacus quis, bibendum ullamcorper velit. Etiam accumsan, diam id efficitur tincidunt, enim turpis rutrum ligula, non mattis ex purus sed magna. Fusce quis magna ac sem tristique vehicula. Maecenas vehicula eget turpis id tincidunt. Maecenas sollicitudin volutpat volutpat. Ut ut nisl vel lacus tempor ornare eu a diam. Nullam semper lacus in dui vulputate convallis. Aenean dignissim tortor tortor, nec rutrum velit ullamcorper nec. Mauris id lectus condimentum, ultricies odio eu, condimentum elit. Ut ut maximus quam, non commodo mi. Cras non tristique enim. Proin bibendum non libero quis viverra.
</p>";
        $page->created_at = Carbon::now()->subDays(4);
        $page->save();


        $page = new Page;
        $page->name = "Terms and Conditions";
        $page->url = str_slug($page->name);
        $page->title = "Términos y condiciones";
        $page->content = "<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt odio vitae pretium consectetur. Morbi eleifend lectus eu nunc scelerisque, ac tempor ipsum cursus. Nullam vitae nulla sodales, laoreet eros sed, mollis mauris. Phasellus vel urna tortor. Etiam vitae quam ultrices, tempus tortor non, venenatis nulla. Fusce vehicula ac purus ut fermentum. Nulla condimentum sem quis pretium consectetur. Integer sollicitudin arcu in metus molestie, at laoreet mauris laoreet. Curabitur eu mollis dolor. Sed commodo ut mi vitae gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Mauris elementum convallis nibh, ac placerat nibh dictum eget. Phasellus iaculis interdum neque, quis volutpat tortor finibus sed.
</p>
<p>
Nam et elementum felis, ac efficitur lacus. Integer tristique diam ante. Proin bibendum vitae lorem a aliquet. Sed ut nisl ut elit viverra imperdiet. In elementum neque quis urna hendrerit maximus. Suspendisse tellus erat, euismod non aliquam eget, eleifend eget diam. Donec imperdiet orci non elit viverra, ac posuere lacus varius. Etiam nulla orci, rutrum vitae pulvinar at, pharetra at est. Pellentesque ipsum quam, rhoncus quis imperdiet id, lacinia nec libero. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum elit augue, pulvinar nec pharetra eu, pulvinar sit amet sem. Aliquam scelerisque, arcu eget sollicitudin volutpat, arcu neque dictum elit, vel dictum urna justo sed mauris. Maecenas vehicula vitae purus in iaculis. Vivamus id dictum magna, eu malesuada odio. Duis accumsan nibh at orci condimentum varius.
</p>
<p>
Etiam tempor egestas ligula, viverra convallis magna viverra a. Ut sed lacus id dui aliquam interdum. Integer ullamcorper luctus quam, nec mollis felis ornare sit amet. Duis tristique vel tellus eget fringilla. Fusce mattis velit in elit lobortis, vitae placerat sem tempus. Sed eu magna condimentum eros viverra efficitur eget malesuada orci. Nam in elementum nisl, ac tincidunt erat. Sed elementum eleifend fermentum. Praesent erat odio, molestie et tristique nec, vestibulum facilisis nisl. Vestibulum sit amet dictum tortor. Vivamus vel gravida odio, vitae egestas massa. Phasellus pretium, dolor vitae semper feugiat, dolor mi dignissim enim, maximus efficitur diam diam eget dolor. Nullam a lacus eu est tincidunt pellentesque quis a nibh. Praesent nec sollicitudin tortor. Aenean vulputate sagittis accumsan. Proin sit amet elit sed purus placerat gravida quis sed ligula.
</p>
<p>
Morbi gravida nunc sit amet erat tempor molestie. Curabitur ultricies neque id metus fermentum semper. Donec rhoncus maximus vestibulum. Donec felis erat, tempor quis lectus ut, dictum euismod purus. Pellentesque finibus velit non vehicula blandit. Praesent tellus odio, dictum ut iaculis eget, condimentum sed turpis. Maecenas faucibus purus a nibh feugiat consequat. Vestibulum pulvinar diam in ipsum tristique, sed fringilla est lobortis. Cras dictum sit amet urna eu efficitur. Mauris lacinia tincidunt viverra. Mauris et accumsan urna. Sed ultricies malesuada ipsum sed fermentum. Aenean non ligula vitae felis viverra volutpat.
</p>
<p>
Morbi cursus ex mattis aliquet posuere. Integer quis tempor quam. In eu leo iaculis, hendrerit lorem id, dignissim lectus. Duis augue ipsum, dictum a lacus quis, bibendum ullamcorper velit. Etiam accumsan, diam id efficitur tincidunt, enim turpis rutrum ligula, non mattis ex purus sed magna. Fusce quis magna ac sem tristique vehicula. Maecenas vehicula eget turpis id tincidunt. Maecenas sollicitudin volutpat volutpat. Ut ut nisl vel lacus tempor ornare eu a diam. Nullam semper lacus in dui vulputate convallis. Aenean dignissim tortor tortor, nec rutrum velit ullamcorper nec. Mauris id lectus condimentum, ultricies odio eu, condimentum elit. Ut ut maximus quam, non commodo mi. Cras non tristique enim. Proin bibendum non libero quis viverra.
</p>";
        $page->created_at = Carbon::now()->subDays(4);
        $page->save();


    }
}
