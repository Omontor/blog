<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::truncate();
        Role::truncate();
        User::truncate();


        $adminRole = Role::create(['name' => 'Admin', 'display_name' => 'Administrador']);
        $writerRole = Role::create(['name' => 'Writer', 'display_name' => 'Escritor']);

        $viewPostsPermission = Permission::create(['name' => 'View Posts', 'display_name' => 'Ver Publicaciones']);
        $createPostsPermission = Permission::create(['name' => 'Create Posts', 'display_name' => 'Crear Publicaciones']);
        $updatePostsPermission = Permission::create(['name' => 'Update Posts', 'display_name' => 'Editar Publicaciones']);
        $deletePostsPermission = Permission::create(['name' => 'Delete Posts', 'display_name' => 'Eliminar Publicaciones']);

        $viewUsersPermission = Permission::create(['name' => 'View Users', 'display_name' => 'Ver Usuarios']);
        $createUsersPermission = Permission::create(['name' => 'Create Users', 'display_name' => 'Crear Usuarios']);
        $updateUsersPermission = Permission::create(['name' => 'Update Users', 'display_name' => 'Editar Usuarios']);
        $deleteUsersPermission = Permission::create(['name' => 'Delete Users', 'display_name' => 'Eliminar Usuarios']);

        $viewRolesPermission = Permission::create(['name' => 'View Roles', 'display_name' => 'Ver Roles']);
        $createRolesPermission = Permission::create(['name' => 'Create Roles', 'display_name' => 'Crear Roles']);
        $updateRolesPermission = Permission::create(['name' => 'Update Roles', 'display_name' => 'Actualizar Roles']);
        $deleteRolesPermission = Permission::create(['name' => 'Delete Roles', 'display_name' => 'Eliminar Roles']);

        $viewPwemissionsPermission = Permission::create(['name' => 'View Permissions', 'display_name' => 'Ver Permisos']);
        $updatePwemissionsPermission = Permission::create(['name' => 'Update Permissions', 'display_name' => 'Actualizar Permisos']);


        $writerRole->givePermissionTo('Create Posts');


        $oliver = new User;
        $oliver->name = 'Oliver Montor';
        $oliver->email = 'montor@me.com';
        $oliver->password= '00000000';
        $oliver->assignRole($adminRole);

        $andrei = new User;
        $andrei->name = 'Andrei Peña';
        $andrei->email = 'andreipena@me.com';
        $andrei->password= '00000000';
        $andrei->assignRole($writerRole);


        $oliver->save();
        $andrei->save();

    }
}
