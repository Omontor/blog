<?php

use App\Post;
use App\Category;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;


class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        File::deleteDirectory(public_path('uploads'));

    	Post::truncate();
    	Category::truncate();
        Tag::truncate();

    	$category = new Category;
    	$category->name = "Categoría 1";
    	$category->save();



    	$category = new Category;
    	$category->name = "Categoría 2";
    	$category->save();


        $category = new Category;
        $category->name = "Categoría 3";
        $category->save();


        $category = new Category;
        $category->name = "Categoría 4";
        $category->save();


        $category = new Category;
        $category->name = "Categoría 5";
        $category->save();


        $post = new Post;
        $post->title = "Mi primer post";
        $post->url = str_slug("Mi primer post");
        $post->excerpt = "Extracto de mi primer post";
        $post->body = "<p> Contenido de mi primer post</p>";
        $post->published_at = Carbon::now()->subDays(4);
        $post->category_id=1;
        $post->user_id=1;
        $post->save();
        $post->tags()->attach(Tag::create(['name'=>'etiqueta 1']));


        $post = new Post;
        $post->title = "Mi Segundo post";
        $post->url = str_slug("Mi segundo post");
        $post->excerpt = "Extracto de mi Segundo post";
        $post->body = "<p> Contenido de mi Segundo post</p>";
        $post->published_at = Carbon::now()->subDays(3);
        $post->category_id=1;
        $post->user_id=2;
        $post->save();
        $post->tags()->attach(Tag::create(['name'=>'etiqueta 2']));

        $post = new Post;
        $post->title = "Mi Tercer post";
        $post->url = str_slug("Mi tercer post");
        $post->excerpt = "Extracto de mi Tercer post";
        $post->body = "<p> Contenido de mi Tercer post</p>";
        $post->published_at = Carbon::now()->subDays(2);
        $post->category_id=2;
        $post->user_id=1;
        $post->save();
        $post->tags()->attach(Tag::create(['name'=>'etiqueta 3']));


        $post = new Post;
        $post->title = "Mi Cuarto post";
        $post->url = str_slug("Mi cuarto post");
        $post->excerpt = "Extracto de mi Cuarto post";
        $post->body = "<p> Contenido de mi Cuarto post</p>";
        $post->published_at = Carbon::now()->subDays(1);
        $post->category_id=2;
        $post->user_id=2;
        $post->save();
        $post->tags()->attach(Tag::create(['name'=>'etiqueta 4']));



    }
}
