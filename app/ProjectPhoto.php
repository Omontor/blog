<?php

namespace App;

use File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ProjectPhoto extends Model
{
    protected $guarded = [];

    protected static function boot(){

    	parent::boot();

    	static::deleting(function($projectphoto){

			File::delete(public_path("/". $projectphoto->url));
	    	});
	}
}
