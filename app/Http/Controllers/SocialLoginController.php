<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Socialite;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\SocialProfile;

class SocialLoginController extends Controller
{
    public function redirectToSocialNetwork ($socialNetwork) {

      if (collect(['twitter' , 'facebook', 'google'])->contains($socialNetwork))
      {

    	return Socialite::driver($socialNetwork)->redirect();

      }


      return redirect()->route('login')->withFlash('No es posible autenticarte con {$socialNetwork}'); 

}


    public function handleSocialNetworkCallback ($socialNetwork) {
	 
         try {

          $socialUser = Socialite::driver($socialNetwork)->user();

          }

          catch (\Exception $e)
          {

          return redirect()->route('login')->withFlash('Error al iniciar sesión');  

          }



      $socialProfile = SocialProfile::firstOrNew([

          'social_network' => $socialNetwork,
          'social_network_user_id' => $socialUser->getID()
        ]);

      	if(!$socialProfile->exists)
      	{
          $user = User::firstOrNew(['email' => $socialUser->getEmail()]);

          if(!$user->exists)
          {
          $user->name = $socialUser->getName();
          $user->assignRole('Writer');
          $user->save();
          }

			    $socialProfile->avatar = $socialUser->getAvatar();
			    $user->profiles()->save($socialProfile);
      	}

      	Auth::login($socialProfile->user);
      	return redirect()->route('dashboard')->withFlash('Sesión iniciada correctamente');
    }
}
