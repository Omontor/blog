<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Media;


class MediaController extends Controller
{

    public function index()
    {
    
        $media =Media::all();
        return view('admin.media.index', compact('media'));
    }

}
