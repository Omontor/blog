<?php

namespace App\Http\Controllers\Admin;

use App\Project;
use App\ProjectCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests\StoreProjectRequest;

class ProjectController extends Controller
{
    public function index()
    {
      
        $projects = Project::all();

        return view('admin.projects.index', compact('projects'));
    }


    public function create () {

        $projectcategories = ProjectCategory::all();
        return view('admin.projects.create', compact('categories'));
    }    

     public function store(Request $request) {

        $this->validate($request,[
          'title' => 'required|min:3']);
        $project = Project::create($request->all());
        return redirect()->route('admin.projects.edit', $project);

    }         


    public function destroy(Project $project){
        
        $project->delete();
         return redirect()->route('admin.projects.index', $project)->with('flash', 'El proyecto ha sido eliminado');

    }


        public function edit(Project $project){
            
            return view('admin.projects.edit',[
           'categories' => ProjectCategory::all(),            
            'project' => $project
            ]);
    }

    public function update (Project $project, StoreProjectRequest $request) {

        $project->update($request->all());
         return redirect()->route('admin.projects.edit', $project)->with('flash', 'La publicación ha sido guardada');

    }  

        

    public function goback () {


         return redirect()->route('admin.projects.index')->with('flash', 'La publicación ha sido guardada');

    } 


}
