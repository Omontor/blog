<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\Category;
use App\Project;
use App\ProjectCategory;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.dashboard', [

                 'users' => User::all()->count(),
                 'posts' => Post::all()->count(),
                 'categories' => Category::all()->count(),
                 'projects' => Project::all()->count(),
                 'projectcategories' => ProjectCategory::all()->count(),
             ]);
    }
}
