<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class UsersPermissionController extends Controller
{
    public function update(Request $request, User $user)
    {

        $user->permissions()->detach();

        if($request->filled('permissions'))
        {

            $user->givePermissionTo($request->permissions);

        }

        return back()->withFlash('Los Permisos Han Sido Actualizados');
    }
}
