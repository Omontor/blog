<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Page;

class StaticPagesController extends Controller
{
     public function show(Page $page) {

   		return view('page.show', compact('page')); 

}
