<?php

namespace App;

use File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Photo extends Model
{
    protected $guarded = [];

    protected static function boot(){

    	parent::boot();

    	static::deleting(function($photo){

			File::delete(public_path("/". $photo->url));
    	});

}


}

