<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectCategory extends Model
{
    protected $guarded = [];


    public function getRouteKeyName() {

    	return 'url';

    }

    public function projects() {


		return $this->hasMany(Project::class);

    }
        public function setNameAttribute($name) {

    		$this->attributes['name'] = $name;
    		$this->attributes['url'] = str_slug($name);

    }
}
