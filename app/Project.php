<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

    protected $dates = ['published_at'];

    protected $fillable = [
 		'title', 'body', 'iframe', 'created_at', 'category_id', 'youtube_id' , 'vimeo_id', 'ivoox_id',
    ];

       protected static function boot(){

    	parent::boot();
    	static::deleting(function($project){

        $project->projectphotos->each->delete();

    	});
    }
   

    public function getRouteKeyName(){


    	return 'url';
    }



	public function category(){

		return $this->belongsTo(ProjectCategory::class);

	}


	public function setCategoryIdAttribute($category){


		$this->attributes['category_id']  = ProjectCategory::find($category) 
        ? $category
        : ProjectCategory::create(['name' => $category])->id; 
	}

	
	public function generateURL() {



		$url = str_slug($this->title);
		if ( $this::whereUrl($url)->exists()) {
			
        $url = "{$url}-{$this->id}";
  
		}

		$this->url = $url;
    	$this->save();
  
	}

	public static function create(array $attributes = []) {

	

		$project = static::query()->create($attributes);

		$project->generateURL();

			return $project;

	}

		public function projectphotos() {

		return $this->hasMany(ProjectPhoto::class);
	}

}
