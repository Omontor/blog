<div class="container container-flex space-between">
            <figure class="logo"><img src="/img/logo.png" alt=""></figure>
            <nav class="custom-wrapper" id="menu">
                <div class="pure-menu"></div>
                <ul class="container-flex list-unstyled">

                    <li><a href="{{route('pages.home')}}" class="text-uppercase  {{setActiveRoute('pages.home')}}">Inicio</a></li>
                    <li><a href="{{route('pages.about')}}" class="text-uppercase  {{setActiveRoute('pages.about')}}">Nosotros</a></li>
                    <li><a href="{{route('pages.archive')}}" class="text-uppercase  {{setActiveRoute('pages.archive')}}">Archivo</a></li>
                    <li><a href="{{route('pages.contact')}}" class="text-uppercase  {{setActiveRoute('pages.contact')}}">Contacto</a></li>

                    @guest
                    <li><a href="/login">Iniciar Sesión</a></li>
                    @endguest
                    @auth
                    <li><a href="/admin" class="text-uppercase">Dashboard</a></li></li>
                    <li>      <form method="POST" action="{{route('logout')}}">
        {{csrf_field()}}
      <button class="btn btn-danger btn-block"
      >Cerrar Sesión</button>
      @endauth
      </form></li>
                </ul>
            </nav>
        </div>