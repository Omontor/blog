@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Usuarios</h1>
            <small>Listado</small>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item active">Usuarios</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')
          <div class="card">
            <div class="card-header" style="vertical-align:  middle;">
              <h3 class="card-title" style="vertical-align:  middle;">Usuarios</h3>
<!-- Button trigger modal -->
@can('create', $users->first())
  <a href="{{route('admin.users.create')}}" class="btn btn-primary float-right">
  + Crear Usuario
  </a>
@endcan
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="users-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre</th>
                  <th>Email</th>
                  <th>Roles</th>
                  <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                	@forelse($users as $user)
                  @can('view', $user)
               <tr>
                 <td>{{$user->id}}</td>
                  <td>{{$user->name}}</td>
                  <td>{{$user->email}}</td>
                  <td>{{$user->getRoleNames()->implode(', ')}}</td>
                  <td>
                    @can('view', $user)
                    <a class="btn btn-default btn-sm" href="{{route('admin.users.show', $user)}}" target="_self"><i class="fas fa-eye"></i></a>
                    @endcan
                  @can('view', $user)
                  	<a class="btn btn-info btn-sm" href="{{route('admin.users.edit', $user)}}"><i class="fas fa-pencil-alt"></i></a>
                    @endcan

                    @can('delete', $user)
                    <form method="POST" action="{{route('admin.users.destroy', $user)}}" style="display: inline;"> 
                      {{ csrf_field() }} {{method_field('DELETE')}}
                      
                    <button class="btn btn-danger btn-sm" href="" onclick="return confirm('Estás Seguro de Querer Eliminar Este Usuario?')"><i class="fas fa-times"></i></button>

                    </form>
                    @endcan

                  </td>

                   </tr>
                   @endcan
                	@empty
                	<tr>
                		<td>No hay datos para mostrar</td>
                	</tr>
                	@endforelse
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
@endsection



@push('styles')
    <!-- DataTables -->
  <link rel="stylesheet" href="/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush

@push('scripts')
<!-- DataTables -->
<script src="/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


<script>
  $(function () {
    $("#example1").DataTable();
    $('#users-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>




@endpush