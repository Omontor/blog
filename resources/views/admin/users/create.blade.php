@extends('admin.layout')
@section('content')

<h1>Crear Usuario</h1>

<div class="row">
	
	<div class="col-md-12">
	<div class="card card-primary">
    	<div class="card-header"></div>
    	<div class="card-body">   
    	<h3 class="box-title">Datos de Usuario</h3>
    	<hr>
    	@if ($errors->any())
    	<ul class="list-group">
        @include('partials.validationmessages') 
    	</ul>
    	@endif
    	<form method="POST" action="{{route('admin.users.store')}}">
    		{{csrf_field()}}
    		<div class="form-group">
    			<label for="name">
    				Nombre:
    			</label>
    			<input name="name" value="{{old('name')}}" class="form-control">
    		</div>
    		<div class="form-group">
    			<label for="email">
    				email:
    			</label>
    			<input name="email" value="{{old('email')}}" class="form-control">
    		</div>
		<div class="row">
    		<div class="form-group col-md-6">
    			<label>Roles</label>
    			@include('admin.roles.checkboxes')
    		</div>

			<div class="form-group col-md-6">
    			<label>Permisos</label>
    			@include('admin.permissions.checkboxes', ['model' => $user])
    		</div>    		
    	</div>

    		<div class="form-group">
    	<span class="help-block"> La contraseña será generada y enviada al nuevo usuario por email</span>
    		</div>
    		<button class="btn btn-primary btn-block">
    				Crear Usuario
    		</button>
    		
    	</form>
    	</div>      

 	</div>
</div>
</div>

@endsection