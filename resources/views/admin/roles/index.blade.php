@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Roles</h1>
            <small>Listado</small>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item active">Roles</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')
          <div class="card">
            <div class="card-header" style="vertical-align:  middle;">
              <h3 class="card-title" style="vertical-align:  middle;">Usuarios</h3>
<!-- Button trigger modal -->
@can('create', $roles->first())
<a href="{{route('admin.roles.create')}}" class="btn btn-primary float-right">
+ Crear Rol
</a>
@endcan
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="roles-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Identificador</th>
                  <th>Nombre</th>
                  <th>Permisos</th>
                  <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                	@forelse($roles as $role)
               <tr>
                 <td>{{$role->id}}</td>
                  <td>{{$role->name}}</td>
                  <td>{{$role->display_name}}</td>
                  <td>{{$role->permissions->pluck('display_name')->implode(', ')}}</td>
                  <td>

                    @can('update' , $role)
                  	<a class="btn btn-info btn-sm" href="{{route('admin.roles.edit', $role)}}"><i class="fas fa-pencil-alt"></i></a>
                    @endcan 
                    @if($role->id !== 1)
                    @can('delete' , $role)
                    <form method="POST" action="{{route('admin.roles.destroy', $role)}}" style="display: inline;"> 
                      {{ csrf_field() }} {{method_field('DELETE')}}
                      
                    <button class="btn btn-danger btn-sm" href="" onclick="return confirm('Estás Seguro de Querer Eliminar Este Rol?')"><i class="fas fa-times"></i></button>

                    </form>
                    @endcan
                      
                    @endif

                  </td>

                   </tr>
                	@empty
                	<tr>
                		<td>No hay datos para mostrar</td>
                	</tr>
                	@endforelse
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
@endsection



@push('styles')
    <!-- DataTables -->
  <link rel="stylesheet" href="/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush

@push('scripts')
<!-- DataTables -->
<script src="/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


<script>
  $(function () {
    $("#example1").DataTable();
    $('#roles-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>




@endpush