@extends('admin.layout')
@section('content')

<div class="row">
	
	<div class="col-md-12">
	<div class="card card-primary">
    	<div class="card-header"></div>
    	<div class="card-body">   
    	<h3 class="box-title">Editar Rol</h3>
    	<hr>
        @include('partials.validationmessages')
    	<form method="POST" action="{{route('admin.roles.update', $role)}}">
    		{{method_field('PUT')}}

            @include('admin.roles.form')

    		<button class="btn btn-primary btn-block">
    				Actualizar Rol
    		</button>
    		
    	</form>
    	</div>      

 	</div>
</div>
</div>

@endsection