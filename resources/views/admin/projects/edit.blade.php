@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Crear Proyecto</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item"><a href="{{route('admin.projects.index')}}">Proyectos</a></li>
              <li class="breadcrumb-item active">Crear</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')

 <div class="row mb-2">
<div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
            </div>      

  <form method="POST" action="{{route('admin.projects.update', $project)}}">
    {{csrf_field()}} {{method_field('PUT')}}
        <div class="card-body">
          <div class="form-group {{$errors->has('title') ? 'is-invalid' : ''}}">
             <label>Portada del proyecto</label>

             <hr>
                  <label>Título del Proyecto</label>
                  <input name="title" 
                  class="form-control" 
                  placeholder="Ingresa el Título de la Publicación" 
                  value="{{old('title', $project->title)}}">
                  </input>

                  <br>  
                  <div class="form-group {{$errors->has('category_id') ? 'is-invalid' : ''}}">
                    
                    <label>Categoría</label>
                    <select name="category_id" class="form-control select2">
                      <option value=""> 
                        Selecciona Una Categoría
                      </option>


                      @forelse($categories as $category)
                      <option value="{{$category->id}}"

                        {{old('category_id', $project->category_id) == $category->id ? 'selected' : '' }}
                        >{{$category->name}}</option>
                      @empty
                      @endforelse
                    </select>
  {!!$errors->first('category_id', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                  </div>


                 {!!$errors->first('title', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                        </div>
                  <div class="form-group {{$errors->has('body') ? 'is-invalid' : ''}}">
                  <label>Contenido de la Publicación</label>
                          <textarea id="editor1" name="body" rows="10" cols="80" placeholder="Introduce el contenido de tu project">
                           {{old('body', $project->body)}}
                      </textarea>
                      {!!$errors->first('body', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>

  <hr>
    <div class="row">
      <div class="col-sm">
          <div class="form-group {{$errors->has('youtube_id') ? 'is-invalid' : ''}}">
                  <label> Youtube ID</label>
                          <textarea id="editor" name="youtube_id" rows="2" class="form-control" placeholder="Introduce código de Youtube">{{old('youtube_id', $project->youtube_id)}}</textarea>
                      {!!$errors->first('youtube_id', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>
      </div>
      <div class="col-sm">
          <div class="form-group {{$errors->has('vimeo_id') ? 'is-invalid' : ''}}">
                  <label>Vimeo ID</label>
                          <textarea id="editor" name="vimeo_id" rows="2" class="form-control" placeholder="Introduce código de Vimeo">{{old('vimeo_id', $project->vimeo_id)}}</textarea>
                      {!!$errors->first('vimeo_id', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>
      </div>
    </div>


      <div class="row">
      <div class="col-sm">
          <div class="form-group {{$errors->has('ivoox_id') ? 'is-invalid' : ''}}">
                  <label>Ivoox ID</label>
                          <textarea id="editor" name="ivoox_id" rows="2" class="form-control" placeholder="Introduce código Ivoox">{{old('ivoox_id', $project->ivoox_id)}}</textarea>
                      {!!$errors->first('ivoox_id', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>
      </div>
      <div class="col-sm">
          <div class="form-group {{$errors->has('iframe') ? 'is-invalid' : ''}}">
                  <label>iframe</label>
                          <textarea id="editor" name="iframe" rows="2" class="form-control" placeholder="Introduce código iframe">{{old('iframe', $project->iframe)}}</textarea>
                      {!!$errors->first('iframe', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>
      </div>
    </div>

    <hr>
                  <div class="form-group">               
                  <div class="dropzone" id="dropzone">            
                  </div>

                </div>
                <div class="form-group">
                 
                      <button type="submit" class="btn btn-success btn-block">
                    Publicar Proyecto
                  </button>
                   </div>
                </div>   
            </div>
          </div>

  </div>
    </form>

 <div class="row mb-2">
  
<div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
            </div>         


@if($project->projectphotos->count())
            <div class="card-body"> 
              <div class="container">
  <div class="row">
     @foreach($project->projectphotos as $photo)

    <div class="col-sm-12 col-md-6 col-lg-3">
      <form method="POST" action="{{route('admin.projectphotos.destroy', $photo)}}">
                {{method_field('DELETE')}} {{csrf_field()}}
                <button class="btn btn-danger btn-md" style="position: absolute;"><i class="fa fa-trash"></i></button>
                <img src="{{url($photo->url)}}" class="img-fluid img-thumbnail">
                    </div>
                  </form>
                    @endforeach
                  </div>
                </div>
              </div>
</div>
</div>
</div>


@endif

@endsection




@push('styles')

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- daterange picker -->
  <link rel="stylesheet" href="/adminlte/plugins/daterangepicker/daterangepicker.css">
    <!-- Select2 -->
  <link rel="stylesheet" href="/adminlte/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endpush

@push('scripts')


<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/min/dropzone.min.js"></script>
<!-- Select2 -->
<script src="/adminlte/plugins/select2/js/select2.full.min.js"></script>
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<!-- bootstrap datepicker -->
<script src="/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
</script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    CKEDITOR.config.height = 355;
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
<script>

    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2({

      tags:true
    })

    //Initialize Select2 Elements
    $('.select2bs4').select2({

      tags:true
    })
  });
</script>
<script>
	
	var myDropzone = new Dropzone('#dropzone', {


		acceptedFiles: 'image/*',
		paramName: 'photo',
		maxFilesize: 4,
		maxFiles: 10,
		url: '/admin/projects/{{$project->url}}/projectphotos',
		headers: {
			'X-CSRF-TOKEN' : '{{csrf_token()}}'
		},
		dictDefaultMessage: 'Arrastra aquí las fotografías de tu publicación. Recuerda que la primera imagen será asignada como encabezado',

	});
	myDropzone.on('error', function(file, res){

		var msg = res.photo[0];
		$('.dz-error-message:last > span').text(msg);
	});
	Dropzone.autoDiscover = false;



  var myDropzone2 = new Dropzone('#dropzone2', {


    acceptedFiles: 'image/*',
    paramName: 'photo',
    maxFilesize: 4,
    maxFiles: 1,
    url: '/admin/projects/{{$project->url}}/projectheader',
    headers: {
      'X-CSRF-TOKEN' : '{{csrf_token()}}'
    },
    dictDefaultMessage: 'Arrastra aquí la portada de de tu publicación',
    
  });
  myDropzone.on('error', function(file, res){

    var msg = res.photo[0];
    $('.dz-error-message:last > span').text(msg);
  });
  Dropzone.autoDiscover = false;


</script>



@endpush

