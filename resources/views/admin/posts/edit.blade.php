@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Crear Post</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item"><a href="{{route('admin.posts.index')}}">Posts</a></li>
              <li class="breadcrumb-item active">Crear</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')
<form method="POST" action="{{route('admin.posts.update', $post)}}">
  {{csrf_field()}} {{method_field('PUT')}}
 <div class="row mb-2">
  
<div class="col-md-8">
        <div class="card card-primary">
            <div class="card-header">
            </div>         
                     <div class="card-body">
                        <div class="form-group {{$errors->has('title') ? 'is-invalid' : ''}}">
                      <label>Título de la Publicación</label>
                <input name="title" 
                class="form-control" 
                placeholder="Ingresa el Título de la Publicación" 
                value="{{old('title', $post->title)}}">
                </input>
               {!!$errors->first('title', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                      </div>
                <div class="form-group {{$errors->has('body') ? 'is-invalid' : ''}}">
                <label>Contenido de la Publicación</label>
                        <textarea id="editor1" name="body" rows="10" cols="80" placeholder="Introduce el contenido de tu post">
                         {{old('body', $post->body)}}
                    </textarea>
                    {!!$errors->first('body', ' <span class="help-block" style= "color:red;">:message</span>')!!}
              </div>


        <div class="form-group {{$errors->has('iframe') ? 'is-invalid' : ''}}">
                <label>Contenido insertado</label>
                        <textarea id="editor" name="iframe" rows="2" class="form-control" placeholder="Introduce código iframe">{{old('iframe', $post->iframe)}}</textarea>
                    {!!$errors->first('iframe', ' <span class="help-block" style= "color:red;">:message</span>')!!}
              </div>
               
              </div>   
          </div>

        </div>

            <div class="col-md-4">
        <div class="card card-primary">
            <div class="card-header">
            </div>
            <div class="card-body">
                  <div class="form-group">
                  <label>Publicar el:</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input name="published_at" 
                    type="text" 
                    class="form-control float-right" 
                    id="datepicker" 
                    value="{{old('published_at', $post->published_at ? $post->published_at->format('m/d/Y') : \Carbon\Carbon::now()->format('m/d/Y') )}}">
                  </div>
                  <!-- /.input group -->
                </div> 

                <div class="form-group {{$errors->has('category_id') ? 'is-invalid' : ''}}">
                  
                  <label>Categorías</label>
                  <select name="category_id" class="form-control select2">
                    <option value=""> 
                      Selecciona Una Categoría
                    </option>


                    @forelse($categories as $category)
                    <option value="{{$category->id}}"

                      {{old('category_id', $post->category_id) == $category->id ? 'selected' : '' }}
                      >{{$category->name}}</option>
                    @empty
                    @endforelse
                  </select>
{!!$errors->first('category_id', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>

                <div class="form-group {{$errors->has('tags') ? 'is-invalid' : ''}}">
                  <label>Etiquetas</label>
                    <select name="tags[]" class="select2" multiple="multiple" data-placeholder="Selecciona Una o Más Etiquetas" style="width: 100%;">

                      @forelse($tags as $tag)
                      <option {{collect(old('tags', $post->tags->pluck('id')))->contains($tag->id) ? 'selected' : ''}} value="{{$tag->id}}">{{$tag->name}}</option>
                      @empty
                      @endforelse
                  </select>
                  {!!$errors->first('tags', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>
                
 
                  <!-- /.input group -->
               
                <div class="form-group {{$errors->has('excerpt') ? 'is-invalid' : ''}}">
                <label>Resumen Publicación</label>
                <textarea name="excerpt" class="form-control" placeholder="Ingresa el Clickbait de la Publicación">{{old('excerpt', $post->excerpt)}}
                </textarea>
                {!!$errors->first('excerpt', ' <span class="help-block" style= "color:red;">:message</span>')!!}
              </div>

              <div class="form-group">
              	
              	<div class="dropzone">
              		
              	</div>
              </div>

              <div class="form-group">
                
                <button type="submit" class="btn btn-primary btn-block">
                  Guardar Publicación
                </button>
              </div>

  </form>
            </div>
          </div>
    </div>
@if($post->photos->count())
    <div class="col-md-12">
  <div class="card card-primary">
      <div class="card-header">
      </div>
            <div class="card-body"> 
              <div class="container">
  <div class="row">
     @foreach($post->photos as $photo)
    <div class="col-sm-12 col-md-6 col-lg-3">
      <form method="POST" action="{{route('admin.photos.destroy', $photo)}}">
                {{method_field('DELETE')}} {{csrf_field()}}
                <button class="btn btn-danger btn-md" style="position: absolute;"><i class="fa fa-trash"></i></button>
                <img src="{{url($photo->url)}}" class="img-fluid img-thumbnail">
                    </div>
                  </form>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>

</div>
@endif





@endsection


@push('styles')

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- daterange picker -->
  <link rel="stylesheet" href="/adminlte/plugins/daterangepicker/daterangepicker.css">
    <!-- Select2 -->
  <link rel="stylesheet" href="/adminlte/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endpush

@push('scripts')


<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/min/dropzone.min.js"></script>
<!-- Select2 -->
<script src="/adminlte/plugins/select2/js/select2.full.min.js"></script>
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<!-- bootstrap datepicker -->
<script src="/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
</script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    CKEDITOR.config.height = 355;
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
<script>

    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2({

      tags:true
    })

    //Initialize Select2 Elements
    $('.select2bs4').select2({

      tags:true
    })
  });
</script>
<script>
	
	var myDropzone = new Dropzone('.dropzone', {


		acceptedFiles: 'image/*',
		paramName: 'photo',
		maxFilesize: 4,
		maxFiles: 10,
		url: '/admin/posts/{{$post->url}}/photos',
		headers: {
			'X-CSRF-TOKEN' : '{{csrf_token()}}'
		},
		dictDefaultMessage: 'Arrastra aquí las fotografías de tu publicación',
	});
	myDropzone.on('error', function(file, res){

		var msg = res.photo[0];
		$('.dz-error-message:last > span').text(msg);
	});
	Dropzone.autoDiscover = false;
</script>
@endpush

