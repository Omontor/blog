@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Editar Página</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item"><a href="{{route('admin.page.index')}}">Páginas</a></li>
              <li class="breadcrumb-item active">Crear</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')
<form method="POST" action="{{route('admin.page.update', $page)}}">
  {{csrf_field()}} {{method_field('PUT')}}
 <div class="row mb-2">
  
<div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
            </div>         
                     <div class="card-body">
              <div class="form-group {{$errors->has('name') ? 'is-invalid' : ''}}">
                                                <label>URL de la Página</label>
                <input name="title" 
                class="form-control" 
                placeholder="Ingresa el Título de la Publicación" 
                value="{{old('title', $page->name)}}" disabled="true">
                <br>
                </input>
                      <label>Título de la Página</label>
                <input name="title" 
                class="form-control" 
                placeholder="Ingresa el Título de la Publicación" 
                value="{{old('title', $page->title)}}">
                </input>
               {!!$errors->first('title', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                      </div>
                <div class="form-group {{$errors->has('content') ? 'is-invalid' : ''}}">
                <label>Contenido de la Página</label>
                        <textarea id="editor1" name="content" rows="10" cols="80" placeholder="Introduce el contenido de tu page">
                         {{old('content', $page->content)}}
                    </textarea>
                    {!!$errors->first('body', ' <span class="help-block" style= "color:red;">:message</span>')!!}
              </div>
              <div class="form-group">
                
                <button type="submit" class="btn btn-primary btn-block">
                  Guardar Publicación
                </button>
              </div>

  </form>
              </div>   
          </div>

        </div>





@endsection


@push('styles')

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- daterange picker -->
  <link rel="stylesheet" href="/adminlte/plugins/daterangepicker/daterangepicker.css">
    <!-- Select2 -->
  <link rel="stylesheet" href="/adminlte/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endpush

@push('scripts')


<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/min/dropzone.min.js"></script>
<!-- Select2 -->
<script src="/adminlte/plugins/select2/js/select2.full.min.js"></script>
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<!-- bootstrap datepicker -->
<script src="/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
</script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    CKEDITOR.config.height = 355;
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>

@endpush

