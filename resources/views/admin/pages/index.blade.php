@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Páginas</h1>
            <small>Listado</small>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item active">Posts</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')
          <div class="card">
            <div class="card-header" style="vertical-align:  middle;">
              <h3 class="card-title" style="vertical-align:  middle;">Listado de Proyectos</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="posts-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre</th>
                  <th>Título</th>
                  <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                  @forelse($pages as $page)
               <tr>
                 <td>{{$page->id}}</td>
                  <td>{{$page->name}}</td>
                  <td>{{$page->title}}</td>
                  <td>

                    <a class="btn btn-default btn-sm" href="{{route('staticpage.show', $page)}}" target="_blank"><i class="fas fa-eye"></i></a>
                    <a class="btn btn-info btn-sm" href="{{route('admin.page.edit', $page)}}"><i class="fas fa-pencil-alt"></i></a>
  
                    
                  </td>

                   </tr>
                  @empty
                  <tr>
                    <td>No hay datos para mostrar</td>
                  </tr>
                  @endforelse
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
@endsection



@push('styles')
    <!-- DataTables -->
  <link rel="stylesheet" href="/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush

@push('scripts')
<!-- DataTables -->
<script src="/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


<script>
  $(function () {
    $("#example1").DataTable();
    $('#posts-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>




@endpush