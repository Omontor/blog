@extends('admin.layout')
@section('content')

<div class="row">
	
	<div class="col-md-12">
	<div class="card card-primary">
    	<div class="card-header"></div>
    	<div class="card-body">   
    	<h3 class="box-title">Editar Permiso</h3>
    	<hr>
        @include('partials.validationmessages')
    	<form method="POST" action="{{route('admin.permissions.update', $permission)}}">
    		{{method_field('PUT')}} {{ csrf_field()}}

            <div class="form-group">
                <label for="name">
                    Identificador:
                </label>
                <input value="{{$permission->name}}" class="form-control" disabled>
            </div>

                <div class="form-group">
                <label for="display_name">
                    Nombre:
                </label>
                <input name="display_name" value="{{old('display_name',$permission->display_name)}}" class="form-control">
            </div>

    		<button class="btn btn-primary btn-block">
    				Actualizar Permiso
    		</button>
    		
    	</form>
    	</div>      

 	</div>
</div>
</div>

@endsection