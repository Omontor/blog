<figure>
	<img src="{{url($post->photos->first()->url)}}" 
	alt="Foto: {{$post->title}}" class="img-responsive" style="object-fit: cover; width: 100%;">
</figure>
