

@extends('layout')
@section('content')


  <!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs); 
  }(document, 'script', 'facebook-jssdk'));</script>


<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v6.0&appId=2556848171241067&autoLogAppEvents=1"></script>

  


</script>

<article class="post container">



 @include($post->viewType())

{{--               @if($post->photos->count() === 1)
             @include('posts.photo')
              @elseif($post->photos->count() > 1)
              <br>
              @include('posts.carousel')
              @elseif($post->iframe)
              @include('posts.iframe')
              @endif --}}

    <div class="content-post">
      @include('posts.header')
      <h1>{{$post->title}}</h1>
        <div class="divider"></div>
        <div class="image-w-text">
          
          <div>
          	{!!$post->body!!}
          </div>
        </div>

        <footer class="container-flex space-between">


          <div class="buttons-social-media-share">
            <ul class="share-buttons">
              <li>  <!-- Your share button code -->
  <div class="fb-share-button" 
    data-href="{{url()->current()}}" 
    data-layout="button"
    data-size ="large">
  </div></li>
            </ul>
          </div>
  @include('posts.tags')

        
      </footer> 
      <div class="comments">
      <div class="divider"></div>
        <div id="disqus_thread"></div>
			@include('partials.disqus-script')
                                
      </div><!-- .comments -->
    </div>
  </article>
@endsection

@push('styles')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@endpush

@push('scripts')
   <script id="dsq-count-scr" src="//zendero.disqus.com/count.js" async></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
@endpush